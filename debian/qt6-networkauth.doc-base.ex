Document: qt6-networkauth
Title: Debian qt6-networkauth Manual
Author: <insert document author here>
Abstract: This manual describes what qt6-networkauth is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/qt6-networkauth/qt6-networkauth.sgml.gz

Format: postscript
Files: /usr/share/doc/qt6-networkauth/qt6-networkauth.ps.gz

Format: text
Files: /usr/share/doc/qt6-networkauth/qt6-networkauth.text.gz

Format: HTML
Index: /usr/share/doc/qt6-networkauth/html/index.html
Files: /usr/share/doc/qt6-networkauth/html/*.html
